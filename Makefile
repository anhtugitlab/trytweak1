
THEOS_DEVICE_IP = 192.168.1.5

ARCHS = arm64 arm64e

TARGET := iphone:clang:latest:7.0
# INSTALL_TARGET_PROCESSES = SpringBoard


include $(THEOS)/makefiles/common.mk

TWEAK_NAME = firsttweak

firsttweak_FILES = Tweak.xm
firsttweak_CFLAGS = -fobjc-arc

include $(THEOS_MAKE_PATH)/tweak.mk

after-install::
	install.exec "sbreload"
